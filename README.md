# Hybrid Code Search

## Overview
Hybrid Code Search is a small Python app to experiment with hybrid search for code. It consists of the following main parts:
- A loader for chunking code files, generating embeddings, and loading them into Pinecone index (provided by the user; the free tier works fine for this) for vector similarity searching
- Another loader for chunking code files and loading them into a BM25 index for keyword searching
- A single hybrid search retriever that combines separate BM25 and vector search retrievers
- A reranker that uses a small sentence transformer model, `BAAI/bge-reranker-base`, to rerank the results from two search types and provide a normalized ranking score for each
- An LLM evaluator for evaluating the relevance of the search results

## Getting started

1. **Install `pyenv`:**
   If you don't have `pyenv` installed, follow the instructions on the [pyenv GitHub page](https://github.com/pyenv/pyenv#installation).
2. **Install the required Python version:**
   ```sh
   pyenv install 3.12
   ```
3.	**Set the local Python version:**
    Navigate to the project directory and set the local Python version:
    ```sh
    cd /path/to/your/project
    pyenv local 3.12
    ```
4. **Create and install a virtual environment**
   ```sh
   pyenv virtualenv 3.12 code-search2
   pyenv activate code-search2
   ```
5. **Install project dependencies**
   ```sh
   pip install -r requirements.txt
   ```

## Usage

### Generating and storing the embeddings in Pinecone
1. Get a Pinecone and OpenAI trial accounts and API keys if you don't already have one
2. Run `> python populate_pinecone.py` in your terminal to create code chunks, generate their embeddings, and store the embeddings in Pinecone

### Running a hybrid code search
1. Run `> python hybrid_search.py` in your terminal to run a hybrid code search. You will be asked for query input in the terminal and you'll get back a list of code chunks that matched, ordered by relevance, and the results of an LLM judge of the relevance of the context.
2. Repeat as desired for subsequent searches

### Swapping out the code files
The project comes with files from the GitLab monolith's `/app` folder, located in the `./data/source_files` directory. You can delete the contents of `./data/source_files` safely and replace them with the directories and files of your choice.