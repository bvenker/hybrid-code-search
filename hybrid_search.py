import os

import pinecone
from llama_index.vector_stores.pinecone import PineconeVectorStore
from pinecone import Pinecone, ServerlessSpec
import openai
from llama_index.core import (QueryBundle, SimpleDirectoryReader,
                              StorageContext, VectorStoreIndex)
from llama_index.core.node_parser import CodeSplitter
from llama_index.core.postprocessor import SentenceTransformerRerank
from llama_index.core.query_engine import RetrieverQueryEngine
from llama_index.core.retrievers import (BaseRetriever, RouterRetriever,
                                         VectorIndexRetriever)
from llama_index.core.schema import NodeWithScore
from llama_index.core.tools import RetrieverTool
from llama_index.embeddings.openai import OpenAIEmbedding
from llama_index.llms.openai import OpenAI
from llama_index.retrievers.bm25 import BM25Retriever
from tree_sitter_languages import get_parser

# Set OpenAI API key
openai.api_key = os.environ["OPENAI_API_KEY"]

# Set Pinecone API key
pc_api_key = os.environ["PINECONE_API_KEY"]

pc = Pinecone(api_key=pc_api_key)

# Allow parallelism for some tasks
os.environ["TOKENIZERS_PARALLELISM"] = "true"

# Initialize the OpenAIEmbedding model
embed_model = OpenAIEmbedding(
    model="text-embedding-3-small"
)


# Initialize the parser for Ruby in a try block because there were some
# silent errors preventing the parser from loading correctly
try:
    parser = get_parser("ruby")
    code_splitter = CodeSplitter(
        language="ruby",
            chunk_lines_overlap=15,  # lines overlap between chunks
            max_chars=1500,  # max chars per chunk
    )
except Exception as e:
    print(f"Error initializing CodeSplitter: {e}")
    exit(1)

# Load documents
documents = SimpleDirectoryReader("./data/source_files", recursive=True, required_exts=[".rb"]).load_data()

# Set Pinecone as the vector store. Vectors are loaded into Pinecone by ./populate_pinecone.py
pinecone_index = pc.Index("gitlab")
vector_store = PineconeVectorStore(pinecone_index=pinecone_index)

# Initialize LLM + node parser
llm = OpenAI(model="gpt-4")
nodes = code_splitter.get_nodes_from_documents(documents)

# Initialize storage context
storage_context = StorageContext.from_defaults(vector_store=vector_store)
storage_context.docstore.add_documents(nodes)

# Initialize vector index from Pinecone index
index = VectorStoreIndex.from_vector_store(
    vector_store=vector_store
)

# We can pass in the index, docstore, or list of nodes to create the retriever
retriever = BM25Retriever.from_defaults(nodes=nodes, similarity_top_k=5)


# Combine BM25 retriever with vector index retriever
vector_retriever = VectorIndexRetriever(index)
bm25_retriever = BM25Retriever.from_defaults(nodes=nodes, similarity_top_k=5)

retriever_tools = [
    RetrieverTool.from_defaults(
        retriever=vector_retriever,
        description="Useful in most cases"
    ),
    RetrieverTool.from_defaults(
        retriever=BM25Retriever,
        description="Useful if searching about specific information"
    )
]

retriever = RouterRetriever.from_defaults(
    retriever_tools=retriever_tools,
    llm=llm,
    select_multi=True
)

# retrieve the top 10 most similar nodes using embeddings
vector_retriever = index.as_retriever(similarity_top_k=10)

# retrieve the top 10 most similar nodes using bm25
bm25_retriever = BM25Retriever.from_defaults(nodes=nodes, similarity_top_k=10)


# Implement custom retriever
class HybridRetriever(BaseRetriever):
    def __init__(self, vector_retriever, bm25_retriever):
        self.vector_retriever = vector_retriever
        self.bm25_retriever = bm25_retriever
        super().__init__()

    def _retrieve(self, query, **kwargs):
        bm25_nodes = self.bm25_retriever.retrieve(query, **kwargs)
        persistent_bm25_nodes = bm25_nodes
        vector_nodes = self.vector_retriever.retrieve(query, **kwargs)

        # combine the two lists of nodes
        all_nodes = []
        node_ids = set()
        for n in persistent_bm25_nodes:
                if n.node.node_id not in node_ids:
                    n.node.metadata['retriever'] = 'BM25'
                    all_nodes.append(n)
                    node_ids.add(n.node.node_id)
        for n in vector_nodes:
            if n.node.node_id not in node_ids:
                n.node.metadata['retriever'] = 'Vector'
                all_nodes.append(n)
                node_ids.add(n.node.node_id)

        # print(all_nodes)
        return all_nodes


index.as_retriever(similarity_top_k=5)

hybrid_retriever = HybridRetriever(vector_retriever, bm25_retriever)

# Implement re-ranker
reranker = SentenceTransformerRerank(top_n=5, model="BAAI/bge-reranker-base")

query = input("Please enter query: ")

retrieved_nodes = hybrid_retriever.retrieve(query)

# Make sure the BM25 scores persist; they get wiped from memory somehow without this step
persistent_nodes = retrieved_nodes


reranked_nodes = reranker.postprocess_nodes(
    persistent_nodes,
    query_bundle=QueryBundle(query)
)

# Prints the results of the code search. Alternatively, to eliminate the reranker step so
# you can see the relevancy scores of the BM25 results and vector results separately, you
# can changed "reranked_nodes" to "persistent_nodes" on lines 157 and 159 below
print("\n\n\n")
print(len(reranked_nodes), "Results")

for n in reranked_nodes:
    print("==================================================================================")
    print("Similarity: ", n.get_score())
    print("Retriever: ", n.metadata.get("retriever"))
    print("File: ", n.metadata.get("file_name"))
    print("text:\n", n.get_text())


## Full query engine – useful if uyo want to see the LLMs response based on the context
# results, but we don't usually want this so it's commented out.

# query_engine = RetrieverQueryEngine.from_args(
#     retriever=hybrid_retriever,
#     node_postprocessors=[reranker],
#     llm=llm,
# )

# response = query_engine.query(
#     "which file contains the applications controller?"
# )

# print("Response: ", response)

## End Query engine

########## Begin DeepEval testing code ##########
# For reference:
# https://docs.confident-ai.com/docs/metrics-contextual-relevancy

rag_application = index.as_query_engine(similarity_top_k=1)

response_object = rag_application.query(query)

from deepeval import evaluate
from deepeval.metrics import ContextualRelevancyMetric
from deepeval.test_case import LLMTestCase

retrieval_context = response_object

from deepeval.integrations.llama_index import \
    DeepEvalContextualRelevancyEvaluator

response_object = rag_application.query(query)

if response_object is not None:
    actual_output = response_object
    retrieval_context = [node.get_content() for node in response_object.source_nodes]

metric = ContextualRelevancyMetric(
    threshold=0.8,
    model="gpt-4o",
    include_reason=True
)
test_case = LLMTestCase(
    input=query,
    actual_output=actual_output,
    retrieval_context=retrieval_context
)

metric.measure(test_case)
print("Response: ", response_object)
print("Metric score: ", metric.score)
print("Reason: ", metric.reason)



########## Legacy output printing ##########
# print("response_object: \n", response_object)

# print("\n\n")
# print("=======================Context Evaluation Results======================")
# print("\n")
# print("Score: ", evaluation_result.score)
# print("Passing: ", evaluation_result.passing)
# print("Feedback: ", evaluation_result.feedback)


# print("Initial retrieval: ", len(retrieved_nodes), " nodes")
# print("================================")
# print("Re-ranked retrieval: ", len(reranked_nodes), "nodes")
# print(reranked_nodes)
# will retrieve context from specific companies


# query = "service pings"
# nodes = hybrid_retriever.retrieve(query)
# print(nodes)

# print(f"Query: {query}")

# for pn in nodes:
#     print("=========================================================================================")
#     print("Score: ", pn.get_score())
#     print(f"Retriever: {pn.node.metadata.get('retriever')}")
#     print("File: ", pn.metadata.get("file_name"))
#     print("Text:\n", pn.get_text())
