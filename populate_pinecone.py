import os

from llama_index.vector_stores.pinecone import PineconeVectorStore
from pinecone import Pinecone
import openai
from llama_index.core import (SimpleDirectoryReader,
                              StorageContext, VectorStoreIndex)
from llama_index.core.node_parser import CodeSplitter
from llama_index.embeddings.openai import OpenAIEmbedding
from tree_sitter_languages import get_parser

# Set OpenAI API key
openai.api_key = os.environ["OPENAI_API_KEY"]

pc_api_key = os.environ["PINECONE_API_KEY"]

pc = Pinecone(api_key=pc_api_key)

os.environ["TOKENIZERS_PARALLELISM"] = "true"

# Initialize the OpenAIEmbedding model
embed_model = OpenAIEmbedding(
    model="text-embedding-3-small"
)


# Initialize the parser for Ruby
try:
    parser = get_parser("ruby")
    code_splitter = CodeSplitter(
        language="ruby",
            chunk_lines_overlap=15,  # lines overlap between chunks
            max_chars=1500,  # max chars per chunk
    )
except Exception as e:
    print(f"Error initializing CodeSplitter: {e}")
    exit(1)

# Load documents
documents = SimpleDirectoryReader("./data/source_files", recursive=True, required_exts=[".rb"]).load_data()

# Store code vectors in Pinecone
pinecone_index = pc.Index("gitlab")
vector_store = PineconeVectorStore(pinecone_index=pinecone_index)


nodes = code_splitter.get_nodes_from_documents(documents)

# Initialize storage context
storage_context = StorageContext.from_defaults(vector_store=vector_store)
storage_context.docstore.add_documents(nodes)

index = VectorStoreIndex(
    nodes=nodes,
    storage_context=storage_context
)